import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class HeaderService {

    constructor(private httpClient: HttpClient) {
    }

    getMenu() {
      // return this.httpClient.get('./menu.json')
        return [
            {name: 'Strona główna', url: 'strona-glowna'},
            {name: 'O nas', url: 'o-nas'},
            {name: 'kontakt', url: 'kontakt'},
        ];
    }
}

import { Component, OnInit } from '@angular/core';
import {HeaderService} from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public parts: any[] = [];
  public menu: any[] = [];
  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.menu = this.headerService.getMenu();

    this.parts = [
      {name: 'zaloguj się', url: 'login'},
      {name: 'dołącz do nas', url: 'rejestration'}
    ];
  }
  }



import { Component, OnInit } from '@angular/core';
import {FooterService} from "./footer.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public parts: any[] = [];
  public menu: any[] = [];
  constructor(private footerService: FooterService) { }

  ngOnInit(): void {
    this.menu = this.footerService.getMenu();
  }

}

import {Component, OnInit} from '@angular/core';
import {HomeService} from "./home.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public recommended: any;

  constructor(private homeService: HomeService) {
  }

  ngOnInit(): void {
    this.homeService.getRecommended().subscribe((response: any) => {
      this.recommended = response;
      console.log(response);
    });
  }

}

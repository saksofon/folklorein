import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactComponent} from './views/contact/contact.component';
import {AboutUsComponent} from './views/about-us/about-us.component';
import {HomeComponent} from './views/home/home.component';
import {PrivacyPolicyComponent} from './views/privacy-policy/privacy-policy.component';
import {RegulationsComponent} from './views/regulations/regulations.component';
import {SearchComponent} from './views/search/search.component';
const routes: Routes = [

  {path: '', redirectTo: 'strona-glowna', pathMatch: 'full'},
  {path: 'o-nas', component: AboutUsComponent },
  {path: 'strona-glowna', component: HomeComponent},
  {path: 'kontakt', component: ContactComponent},
  {path: 'polityka-prywatnosci', component: PrivacyPolicyComponent },
  {path: 'regulamin', component: RegulationsComponent},
  {path: 'search', component: SearchComponent },
  {path: 'rejestracja', component: HomeComponent},
  { path: '**', redirectTo: 'strona-glowna' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
